package casper.app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import casper.app.common.AppController
import casper.personviewer.R
import com.google.android.material.textfield.TextInputEditText

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val lstUsers = findViewById<RecyclerView>(R.id.lstUsers)
        lstUsers.adapter = PersonListView()
        lstUsers.layoutManager = LinearLayoutManager(this)

        val inputPersonName = findViewById<TextInputEditText>(R.id.textInputPersonName)
        inputPersonName?.doOnTextChanged { text, start, before, count ->
            fillPersonList(text?.toString())
        }
        fillPersonList(inputPersonName?.text?.toString())

        AppController.settings.scrollState?.let { lstUsers.layoutManager?.onRestoreInstanceState(it) }
    }

    private fun fillPersonList(filter: String?) {
        val lstUsers = findViewById<RecyclerView>(R.id.lstUsers)
        val persons = AppController.persons.getPersons()
        val adapter = lstUsers.adapter as? PersonListView ?: return

        if (filter == null || filter == "") {
            //  by default use alphabetical sort
            adapter.persons = persons.sortedBy { it.name }
        } else {
            //  We can use more complex filter for first and last name, use weights, and check errors in input filter...
            //  For more complex filters, it is recommended to separate the logic into a separate class
            //  but now use simple filter
            adapter.persons = persons.filter { it.name.startsWith(filter) }
        }
    }

    override fun onPause() {
        val lstUsers = findViewById<RecyclerView>(R.id.lstUsers)
        AppController.settings.scrollState = lstUsers.layoutManager?.onSaveInstanceState()
        super.onPause()
    }
}