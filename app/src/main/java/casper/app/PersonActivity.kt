package casper.app

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import casper.app.common.AppController
import casper.util.Localize
import casper.personviewer.R

//  TODO: Похож на PersonItemView
class PersonActivity : AppCompatActivity() {
    companion object {
        const val PERSON_ID = "person_id"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_person)

        val personId = intent.getLongExtra(PERSON_ID, 0)
        val person = AppController.persons.getPersons().firstOrNull { it.id == personId }

        val txtId = findViewById<TextView>(R.id.personId)
        val txtName = findViewById<TextView>(R.id.personName)
        val txtBirthday = findViewById<TextView>(R.id.personBirthday)
        val txtRegisterTime = findViewById<TextView>(R.id.personRegisterTime)

        if (person != null) {
            txtId.text = person.id.toString()
            txtName.text = person.name
            txtBirthday.text = Localize.formatDate(person.birthday)
            txtRegisterTime.text = Localize.formatDate(person.registerTime)
        }
    }
}