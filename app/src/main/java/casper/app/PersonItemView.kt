package casper.app

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import casper.app.common.AppController
import casper.core.Person
import casper.personviewer.R

class PersonItemView(view: View) : RecyclerView.ViewHolder(view) {
    private val txtId = view.findViewById<TextView>(R.id.personItemId)
    private val txtName = view.findViewById<TextView>(R.id.personItemName)
    private var current:Person? = null

    init {
        itemView.setOnClickListener {
            onClick()
        }
    }

    private fun onClick() {
        current?.let {
            AppController.showPersonDetail(itemView.context, it)
        }
    }

    fun bind(person: Person?) {
        current = person
        if (person == null) {
            txtId.text = "?"
            txtName.text = "?"
        } else {
            txtId.text = person.id.toString()
            txtName.text = person.name
        }
    }
}