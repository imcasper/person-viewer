package casper.app

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import casper.core.Person
import casper.core.PersonProvider
import casper.personviewer.R

class PersonListView() : RecyclerView.Adapter<PersonItemView>() {
    var persons: List<Person> = emptyList()
        set(value) {
            if (field == value) return
            field = value
            this.notifyDataSetChanged()
        }


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): PersonItemView {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.person_item_view, viewGroup, false)

        return PersonItemView(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: PersonItemView, position: Int) {
        val person = persons.getOrNull(position)
        viewHolder.bind(person)
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = persons.size
}