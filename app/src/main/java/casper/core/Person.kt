package casper.core

import java.util.*

class Person(val id:Long, val name:String, val registerTime:Date, val birthday:Date)