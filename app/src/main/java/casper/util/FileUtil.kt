package casper.util

import casper.app.MainActivity
import java.io.BufferedReader
import java.io.InputStreamReader

object FileUtil {
    fun readLines(fileName: String):List<String> {
        val classLoader = MainActivity::class.java.classLoader
                ?: throw Error("Invalid classLoader")

        try {
            val inputStream = classLoader.getResourceAsStream(fileName)
            val streamReader = InputStreamReader(inputStream)
            BufferedReader(streamReader).use { reader ->
                val lines = mutableListOf<String>()
                while (true) {
                    val line = reader.readLine() ?: return lines
                    lines += line
                }
            }
        } catch (e:Throwable) {
            println("Can't read file $fileName")
            throw e
        }
    }
}