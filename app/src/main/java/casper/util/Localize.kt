package casper.util

import java.text.SimpleDateFormat
import java.util.*

object Localize {
    fun formatDate(date:Date):String {
        return SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(date)
    }
}